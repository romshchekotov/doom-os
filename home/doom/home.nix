{
  home = {
    username = "doom";
    homeDirectory = "/home/doom";
    stateVersion = "22.11";
    extraOutputsToInstall = ["doc" "devdoc"];
  };

  manual = {
    html.enable = true;
    json.enable = false;
    manpages.enable = true;
  };

  # let HM manage itself when in standalone mode
  programs.home-manager.enable = true;
}
