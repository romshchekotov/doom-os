{
  config,
  pkgs,
  lib,
  ...
}: {
  home.packages = with pkgs; [
    catimg
    duf
    du-dust
    fd
    file
    joshuto
    ranger
    ripgrep
    yt-dlp
  ];

  programs = {
    bat.enable = true;
    exa.enable = true;

    fzf = {
      enable = true;
    };

    zoxide = {
      enable = true;
    };

    dircolors = {
      enable = true;
    };
  };
}
