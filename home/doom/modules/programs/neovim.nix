{
  config,
  pkgs,
  ...
}: {
  programs.neovim = {
    enable = true;
    extraPackages = with pkgs; [
      rnix-lsp nixpkgs-fmt
    ];
    coc = {
      enable = true;
      settings = {
        languageserver = {
          nix = {
            command = "rnix-lsp";
            filetypes = ["nix"];
          };
        };
      };
    };
    plugins = with pkgs.vimPlugins; [
      # Libraries / Dependencies
      plenary-nvim
      # Theming
      nvim-web-devicons
      feline-nvim
      # Color Schemes
      { plugin = catppuccin-nvim; config = "colorscheme catppuccin"; }
      aurora
      # Explore
      nvim-tree-lua
      telescope-nvim
      telescope-lsp-handlers-nvim
      telescope-fzf-native-nvim
      telescope-file-browser-nvim
      telescope-coc-nvim
      bufferline-nvim
      # Language Support
      vim-nix
      vimtex
    ];
    extraConfig = ''
      lua << EOF
      local set = vim.opt

      set.number = true
      set.tabstop = 2
      set.shiftwidth = 2
      set.softtabstop = 2
      set.expandtab = true
      set.hlsearch = true
      set.incsearch = true
      set.ignorecase = true
      set.smartcase = true
      set.list = true
      set.listchars = { tab = '▸ ', trail = '·' }
      set.termguicolors = true

      vim.cmd('syntax enable')

      require("nvim-tree").setup({
        sort_by = "case_sensitive",
        view = {
          adaptive_size = true,
          mappings = {
            list = {
              { key = "u", action = "dir_up" },
            },
          },
        },
        renderer = {
          group_empty = true,
        },
        filters = {
          dotfiles = true,
        },
      })

      require('feline').setup()
      require('bufferline').setup()

      vim.g.mapleader = ' '
      local keyset = vim.keymap.set
      local opts = {silent = true, noremap = true, expr = true, replace_keycodes = false}

      function _G.check_back_space()
        local col = vim.fn.col('.') - 1
        return col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') ~= nil
      end

      keyset('n', '<leader>.', ':NvimTreeToggle<cr>')
      keyset('n', '<leader>f', ':Telescope fd<cr>')
      keyset('n', '<leader>g', ':Telescope live_grep<cr>')
      keyset('n', '<leader>h', '<c-w><c-h>')
      keyset('n', '<leader>j', '<c-w><c-j>')
      keyset('n', '<leader>k', '<c-w><c-k>')
      keyset('n', '<leader>l', '<c-w><c-l>')

      keyset('i', '<tab>', 'coc#pum#visible() ? coc#pum#next(1) : v:lua.check_back_space() ? "<tab>" : coc#refresh()', opts)
      keyset('i', '<s-tab>', [[coc#pum#visible() ? coc#pum#prev(1) : "\<c-h>"]], opts)
      keyset('i', '<cr>', [[coc#pum#visible() ? coc#pum#confirm() : "\<c-g>u\<cr>\<c-r>=coc#on_enter()\<cr>"]], opts)
      keyset('i', '<c-space>', 'coc#refresh()', {silent = true, expr = true})
      keyset('n', '<leader>qf', '<Plug>(coc-fix-current)', { silent = true, nowait = true })
      EOF
    '';
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    withNodeJs = true;
    withPython3 = true;
    withRuby = true;
  };
}
