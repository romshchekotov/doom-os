{
  imports = [
    ./kitty.nix
    ./mpd.nix
    ./neovim.nix
    ./zathura.nix
  ];
}
