{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.modules.nixos.virtualisation.podman;
in {
  options.modules.nixos.virtualisation.podman = {
    enable = mkEnableOption "Enable The Podman Container Engine";
  };

  config = mkIf cfg.enable {
    virtualisation.podman = {
      enable = true;
      extraPackages = with pkgs; [
        skopeo
        conmon
        runc
      ];
    };
  };
}
