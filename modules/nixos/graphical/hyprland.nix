{
  config,
  pkgs,
  lib,
  ...
}: with lib; let
  cfg = config.modules.nixos.graphical.hyprland;
in {
  options.modules.nixos.graphical.hyprland = {
    enable = mkEnableOption "Enable the Hyprland Window Manager";
  };

  config = mkIf cfg.enable {
    programs = {
      hyprland = {
        enable = true;
        xwayland = {
          enable = true;
          hidpi = true;
        };
      };
      # TODO: Create Configuration based on 'option's
    };
  };
}
