{
  config,
  pkgs,
  lib,
  ...
}: with lib; let
  cfg = config.modules.nixos.bootloader.grub;
in {
  options.modules.nixos.bootloader.grub = {
    enable = mkEnableOption "Enable The Grub Bootloader";

    isUEFISystem = mkOption {
      type = types.bool;
      default = true;
      description = "Whether we're on a multi-partition UEFI System.";
    };

    efiSysMountPoint = mkOption {
      type = types.str;
      default = "/boot";
      description = "The mount point of the EFI System Partition";
    };

    device = mkOption {
      type = types.str;
      default = "nodev";
      description = "The device to install grub to";
    };

    useOSProber = mkOption {
      type = types.bool;
      default = true;
      description = "Whether to use os-prober to detect other operating systems";
    };
  };

  config = mkIf cfg.enable {
    boot.loader = (mkMerge [ 
      (mkIf cfg.isUEFISystem {
        efi = {
          canTouchEfiVariables = true;
          efiSysMountPoint = "${cfg.efiSysMountPoint}";
        };
      })
      { systemd-boot.enable = false; }
      { grub = (mkMerge [ 
        {
          enable = true;
          version = 2;
          device = "${cfg.device}";
        }
        (mkIf cfg.isUEFISystem {
          efiSupport = true;
          enableCryptodisk = true;
        })
        {
          useOSProber = cfg.useOSProber;
          configurationLimit = 8;
          gfxmodeEfi = "1920x1080";
          theme = pkgs.fetchzip {
            url = "https://raw.githubusercontent.com/AdisonCavani/distro-grub-themes/master/themes/nixos.tar";
            hash = "sha256-OmFDyktTc/l+3wHboHeFpAQgPt3r7jjqZf8MrwuUGMo=";
            stripRoot = false;
          };
        }
      ] ); }
    ] );
  };
}
