{
  description = "Doomie's Nix Config";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    hyprland.url = "github:hyprwm/Hyprland";
    hyprpaper.url = "github:hyprwm/hyprpaper";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    hyprland,
    ...
  } @ inputs : let
    inherit (self) outputs;
    system = "x86_64-linux";
    lib = nixpkgs.lib;
    pkgs = import nixpkgs {
      inherit system;
      config.tarball-ttl = 0;
    };
  in rec {
    inherit lib pkgs;
    nixOsModules = import ./modules/nixos;
    nixosConfigurations = import ./hosts {inherit inputs outputs;};
  };
}
