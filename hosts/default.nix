# Depends on [home/doom/default.nix, hosts/nihil/default.nix]
{
  inputs,
  outputs,
  ...
}: let
  sharedModules =
    [
      inputs.home-manager.nixosModules.home-manager
      {
        home-manager = {
          useGlobalPkgs = true;
          useUserPackages = true;
          extraSpecialArgs = {inherit inputs outputs;};
          users.doom = ../home/doom;
        };
      }
    ]
    ++ (builtins.attrValues outputs.nixOsModules);
in {
  nihil = outputs.lib.nixosSystem {
    modules =
      [
        inputs.hyprland.nixosModules.default
        {networking.hostName = "nihil";}
        ./nihil
      ]
      ++ sharedModules;

    specialArgs = {inherit inputs;};
    system = "x86_64-linux";
  };
}
