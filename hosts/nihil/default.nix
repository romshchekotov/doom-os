# Depends on: [
#   hosts/nihil/hardware-configuration.nix,
#   hosts/shared/default.nix (!)
# ]
{
  config,
  lib,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
    ../shared
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    initrd = {
      systemd.enable = true;
      supportedFilesystems = ["ext4"];
    };
  };

  hardware = {
    bluetooth = {
      enable = true;
      package = pkgs.bluez;
    };

    pulseaudio.enable = false;
  };

  services = {
    acpid.enable = true;
    thermald.enable = true;
    upower.enable = true;

    tlp = {
      enable = true;
      settings = {
        START_CHARGE_THRESH_BAT0 = 0;
        STOP_CHARGE_THRESH_BAT0 = 80;
      };
    };
  };

  environment = {
    sessionVariables = {
      WLR_RENDERER_ALLOW_SOFTWARE = "1";
      _JAVA_AWT_WM_NONREPARENTING = "1";
    };
    variables = {
      __GL_MaxFramesAllowed = "0";
    };

    systemPackages = with pkgs; [
      acpi
      brightnessctl
      libva-utils
      ocl-icd
      vulkan-tools
    ];
  };

  modules.nixos = {
    bootloader.grub = {
      enable = true;
      isUEFISystem = false;
      device = "/dev/vda";
    };

    virtualisation = {
      docker.enable = true;
      libvirtd.enable = true;
      podman.enable = true;
    };

    graphical.hyprland = {
      enable = true;
    };
  };

  system.stateVersion = lib.mkForce "22.11";
}
