{
  lib,
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./environment
    ./pkgs
    ./programs
    ./security
    ./services
    ./system
  ];

  programs = {
    dconf.enable = true;
    nm-applet.enable = true;
    seahorse.enable = true;
  };

  services = {
    blueman.enable = true;
    fstrim.enable = true;
    fwupd.enable = true;
    gvfs.enable = true;
    udisks2.enable = true;
    printing.enable = true;
  };
}
