{
  pkgs,
  config,
  lib,
  ...
}: let
  ifExists = groups: builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
in {
  users.mutableUsers = true;
  users.users.doom = {
    description = "Roman Shchekotov";
    isNormalUser = true;
    shell = pkgs.bash;
    initialPassword = "nixos";
    extraGroups =
      [
        "wheel"
        "networkmanager"
        "video"
        "audio"
        "nix"
        "systemd-journal"
      ]
      ++ ifExists [
        "docker"
        "podman"
        "git"
        "libvirtd"
        "mysql"
      ];
      uid = 1000;
    openssh.authorizedKeys.keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINaC2z45pyRZvhnev1aeJMf4u2iKo95CPh+9Pi3Xs3VK doom@doom"];
  };
}
