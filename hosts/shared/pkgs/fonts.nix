{
  pkgs,
  lib,
  ...
}: {
  fonts = {
    fonts = lib.attrValues {
      inherit (pkgs)
        inter
        liberation_ttf
        material-design-icons
        noto-fonts
        noto-fonts-cjk
        noto-fonts-emoji
        victor-mono;
    } ++ [ ( pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; } ) ];

    fontconfig = {
      enable = true;
      antialias = true;
      hinting = {
        enable = true;
        autohint = true;
        style = "hintfull";
      };

      subpixel.lcdfilter = "default";

      defaultFonts = {
        emoji = ["Noto Color Emoji"];
        monospace = ["FiraCode"];
        sansSerif = ["Noto Sans" "Noto Color Emoji"];
        serif = ["Noto Serif" "Noto Color Emoji"];
      };
    };
  };
}
