{
  imports = [
    ./shell.nix
    ./syspkgs.nix
    ./variables.nix
  ];
}
