{
  imports = [
    ./dbus.nix
    ./gnome.nix
    ./logind.nix
    ./pipewire.nix
    ./printing.nix
    ./ssh.nix
  ];
}
