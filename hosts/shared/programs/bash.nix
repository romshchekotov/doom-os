{ pkgs, ... }: let
  # (Echo-)Colors
  extra_color = "\\[\\033[1;35m\\]";
  eextra_color = "\\001\\033[1;35m\\002";
  user_color = "\\[\\033[1;97m\\]";
  loc_color = "\\[\\033[1;94m\\]";
  # (Echo-)Formatting
  clear = "\\[\\033[0m\\]";
  eclear = "\\001\\033[0m\\002";
  ebold = "\\001\\033[1m\\002";
  # (Commands)
  git_branch = "\\$(__git_ps1)";
in {
  programs.bash = {
    shellAliases = {
      suvim = "sudo -E vim";

      hsi = "history | grep";
      cat = "bat";
      ls = "exa -al --color=always --group-directories-first";
      la = "exa -a --color=always --group-directories-first";
      ll = "exa -l --color=always --group-directories-first";
      lt = "exa -aT --color=always --group-directories-first";
      "l." = "exa -a | egrep '^\.'";

      rebuild = "sudo nixos-rebuild switch";
      nixconf = "(cd /etc/nixos && sudo -E vim /etc/nixos/flake.nix)";
    };
    interactiveShellInit = ''
      # https://github.com/NixOS/nixpkgs/issues/3990#issuecomment-260204208
      source ${pkgs.gitAndTools.gitFull}/share/git/contrib/completion/git-prompt.sh

      # https://direnv.net/docs/hook.html
      eval "$(direnv hook bash)"
    '';
    promptInit = ''
      # https://stackoverflow.com/questions/19092488/custom-bash-prompt-is-overwriting-itself
      # https://gist.github.com/brettinternet/0d2225ffb6b224c515643f630a65b463
      # https://web.archive.org/web/20160704140739/http://ithaca.arpinum.org/2013/01/02/git-prompt.html

      if [ "$TERM" != "dumb" ] || [ -n "$INSIDE_EMACS" ]; then
        BASE_PROMPT="\n${extra_color}➜ ${user_color}\\u ${loc_color}\\W${clear}";
        GIT_PROMPT=" \$( \
          [[ -z \$(__git_ps1) ]] && \
          echo || \
          printf '${eextra_color}git:(${eclear}${ebold}%s${eclear}${eextra_color})${eclear}' \$(git branch | grep '^*' | grep -oE '[^* ]+') \
        )";
        PS1="$BASE_PROMPT$GIT_PROMPT ";
      fi
    '';
  };
}
